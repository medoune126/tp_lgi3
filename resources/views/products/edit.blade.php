@extends('layout')

@section('title', "Modification d'un produit")

@section('content')
    <h3>Modification d'un nouveau produit</h3>
    <form action="{{ route('products.update',$product->id) }}" method="post">
        @csrf
        @method('PUT');
        @include('products.form')

        <div class="d-grid gap-2">
            <button type="submit" class="btn btn-primary">
                Modifier le produit
            </button>
        </div>
    </form>
@endsection