<div class="form-floating mb-3">
    <input 
        value="@isset($category->name) {{ $category->name }} @endisset"
        required type="text" class="form-control" 
            id="name" name="name" placeholder="Ex : Cosmétiques">
    <label for="name">
        Nom de la catégorie
    </label>
</div>