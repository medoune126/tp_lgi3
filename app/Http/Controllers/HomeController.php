<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home($nom){
        return view('home',compact('nom'));
    }
}
